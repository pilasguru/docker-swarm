#!/bin/bash

docker-machine create --driver virtualbox manager1
docker-machine create --driver virtualbox manager2
docker-machine create --driver virtualbox worker1
docker-machine create --driver virtualbox worker2
docker-machine create --driver virtualbox worker3

echo
echo
sleep 5
docker-machine ls
