#!/bin/bash

# Requires:
# - credentials at ~/.aws/credentials
# - vpc, zone and subnet ajustments

AMI=ami-cd0f5cb6

# AZ a subnet-bbed25cc
docker-machine create --driver amazonec2 \
	--amazonec2-region us-east-1 \
 	--amazonec2-zone a \
 	--amazonec2-vpc-id vpc-3e9e365b \
    --amazonec2-subnet-id subnet-bbed25cc \
	--amazonec2-ami $AMI \
    manager1

# AZ b subnet-41ce2418
docker-machine create --driver amazonec2 \
	--amazonec2-region us-east-1 \
	--amazonec2-zone b \
	--amazonec2-vpc-id vpc-3e9e365b \
	--amazonec2-subnet-id subnet-41ce2418 \
	--amazonec2-ami $AMI \
	manager2

# Workers at different AZ
docker-machine create --driver amazonec2 \
	--amazonec2-region us-east-1 \
 	--amazonec2-zone a \
 	--amazonec2-vpc-id vpc-3e9e365b \
    --amazonec2-subnet-id subnet-bbed25cc \
	--amazonec2-ami $AMI \
    worker1

docker-machine create --driver amazonec2 \
	--amazonec2-region us-east-1 \
	--amazonec2-zone b \
	--amazonec2-vpc-id vpc-3e9e365b \
	--amazonec2-subnet-id subnet-41ce2418 \
	--amazonec2-ami $AMI \
	worker2

docker-machine create --driver amazonec2 \
	--amazonec2-region us-east-1 \
	--amazonec2-zone e \
	--amazonec2-vpc-id vpc-3e9e365b \
	--amazonec2-subnet-id subnet-32cfc51a \
	--amazonec2-ami $AMI \
	worker3
