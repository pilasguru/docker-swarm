# Docker Swarm Presentation

This is the presentation presentation about *Docker Swarm* made for the `#fridaytalks` at [Moove-it](https://moove-it.com) on October 6, 2017.

## Youtube

* [Friday Talks - Docker Swarm by Rodolfo Pilas](https://www.youtube.com/watch?v=X2GeTQqzLCY)

## Scripts

The `bin/` folder has scripts to startup basic installation using [docker-machine](https://docs.docker.com/machine/overview/)

## Demo Videos

The `video/` folder has the dump for [asciinema](https://asciinema.org)'s videos, play it with:

```
asciinema play videos/05-update.json
```

command.

## References

* [Docker Swarm For High Availability | Docker Tutorial | DevOps Tutorial | Edureka](https://www.youtube.com/watch?v=Ceqb53EXANk)
* [15 minutes with Docker Swarm Mode](https://medium.com/@LachlanEvenson/15-minutes-with-docker-swarm-mode-e6c38b9dafa0)


